// ==UserScript==
// @name        Remove animation style from Steam sale pages
// @namespace   remove-animation-on-steam-sales
// @match       https://store.steampowered.com/sale/*
// @icon        https://store.steampowered.com/favicon.ico
// @version     1.0
// @author      xPaw
// @grant       GM_addStyle
// ==/UserScript==

GM_addStyle( 'body { animation: none !important; }' );